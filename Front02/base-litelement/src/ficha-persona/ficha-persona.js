import { LitElement, html} from 'lit-element';

class FichaPersona extends LitElement {


    static get properties(){
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            photo: {type: Object}
        };
    }

    constructor(){
        super();

        this.name = "Prueba Nombre";
        this.yearsInCompany = 12;
        this.photo = {
            src: "./src/ficha-persona/download.png",
            alt: "Foto persona"
        }

    }


    render(){
        return html`
            <div>
                <label for="fname">Nombre Completo</label>
                <input type="text" id="fname" name="fname" value="${this.name}"></input>
                <br/>
                <label for="age">Años en la empresa</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}"></input>
                <br/>
                <img src="${this.photo.src}" height="200" width="200" alt="${this.photo.alt}">
            </div>
        `;
    }
}

customElements.define('ficha-persona', FichaPersona);
