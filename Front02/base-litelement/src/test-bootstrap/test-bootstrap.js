import { LitElement, html} from 'lit-element';

class TestBootStrap extends LitElement {
    render(){
        return html`
            <div>Test Bootstrap!</div>
        `;
    }
}

customElements.define('test-bootstrap', TestBootStrap);
